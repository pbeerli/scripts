#!/usr/bin/env
#
import sys
import os

filename = sys.argv[1]

def pairs_1(t):
    return zip(t[::2], t[1::2]) 


def read_phylip(filename):
    all=[]
    missing=[]
    names=[]
    f=open(filename,'r')
    first = f.readline().rstrip().split()
    ind, loci = first[0], first[1]
    numloci = f.readline().rstrip().split()
    if loci != len(numloci):
        print >> sys.stderr, "WARNING", loci, len(numloci)
    for fi in f:
        line = fi
        line = line.rstrip()
        larray = line.split()
        #assuming two alleles per locus
        alleles=pairs_1(larray[1:])
        locus = 0
        print >> sys.stderr, larray[0],
        for ai in alleles:
            sum = float(ai[0]) + float(ai[1])
            locus += 1
            if sum < 1.0 - 0.0001 or sum > 1.0 :
                print >> sys.stderr, "(%4d %f %s %s)" % (locus,sum, ai[0],ai[1])
                missing.append(locus-1)
        names.append(larray[0])
        all.append(alleles)
    return int(ind), int(loci), names, all, sorted(list(set(missing)))
        
    f.close()


ind, loci, names, all, miss = read_phylip(filename)
print >> sys.stderr, miss
newloci = loci-len(miss)
print ind, newloci
for i in xrange(newloci):
    print 2,
print
for ni,ai in zip(names,all):
    print "%-10.10s" % ni,
    for i in xrange(loci):
        if i in miss:
            continue
        else:
            print "%1.10f %1.10f" % (float(ai[i][0]), float(ai[i][1])),
#            print "(%4li) %1.10f %1.10f" % (i, float(ai[i][0]), float(ai[i][1])),
    print


        
